package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class connect {
    private String dbname;
    private String ip;
    private String username;
    private String password;

    private Connection db;

    public connect(String dbname, String ip, String username, String password) throws SQLException
    {
        this.dbname = dbname;
        this.ip = ip;
        this.username = username;
        this.password = password;
        this.conn();
    }

    private void conn() throws SQLException
    {
        //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        this.db = DriverManager.getConnection(
                "jdbc:mysql://"+this.getIp()+"/"+this.getDbname(),
                this.getUsername(),
                this.getPassword()
        );
        this.db.setAutoCommit(false);
    }

    private String getDbname() {
        return dbname;
    }

    private String getIp() {
        return ip;
    }

    protected String getUsername() {
        return username;
    }

    protected String getPassword() {
        return password;
    }

    public Connection getDb() {
        return db;
    }
}
