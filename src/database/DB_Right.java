package database;

import data.RightManager.Right;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DB_Right {


    public static Right getRightByLibelle(String right) throws SQLException
    {
        Connection db = new connect(
                credentials.getDbname(),
                credentials.getDbHost(),
                credentials.getDbUser(),
                credentials.getDbpass()
        ).getDb();

        String right_req = "SELECT * FROM Droit WHERE libelle LIKE ?";

        PreparedStatement right_stmt = db.prepareStatement(right_req);

        right_stmt.setString(1, right.toLowerCase());

        ResultSet right_rs = right_stmt.executeQuery();

        if (!right_rs.first())
            throw new SQLException("Right Not Found");

        Right R = new Right(right_rs.getInt("id"), right_rs.getString("libelle"));


        right_rs.close();
        right_stmt.close();
        db.close();

        return R;
    }
}
