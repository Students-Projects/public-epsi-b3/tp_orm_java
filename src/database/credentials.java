package database;

public class credentials {
    private static String dbname = "EPSI_JAVA_ORM";
    private static String dbpass = "epsi";
    private static String dbUser = "epsi";
    private static String dbHost = "127.0.0.1";

    public static String getDbname() {
        return dbname;
    }

    public static String getDbpass() {
        return dbpass;
    }

    public static String getDbUser() {
        return dbUser;
    }

    public static String getDbHost() {
        return dbHost;
    }
}
