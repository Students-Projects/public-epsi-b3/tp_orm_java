package externalFunctions;

import data.UserManager.User;
import database.connect;
import database.credentials;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class user_rights
{
    public static boolean isAutorise(String login, String right) throws SQLException
    {
        Connection db = new connect(
                credentials.getDbname(),
                credentials.getDbHost(),
                credentials.getDbUser(),
                credentials.getDbpass()
        ).getDb();


        String request = "SELECT id FROM Utilisateur WHERE login LIKE ?";
        String right_req = "SELECT id FROM Droit WHERE libelle LIKE ?";

        PreparedStatement pstmt = db.prepareStatement(request);
        PreparedStatement right_stmt = db.prepareStatement(right_req);

        pstmt.setString(1, login.toLowerCase());
        right_stmt.setString(1, right.toLowerCase());

        ResultSet rs = pstmt.executeQuery();
        ResultSet right_rs = right_stmt.executeQuery();

        if (!rs.first())
            throw new SQLException("Login Not Found");

        if (!right_rs.first())
            throw new SQLException("Right Not Found");

        String ud_req = "SELECT * FROM Utilisateur_Droit WHERE id_droit = ? AND id_utilisateur = ?";
        PreparedStatement ud_stmt = db.prepareStatement(ud_req);

        ud_stmt.setString(1, right_rs.getString("id"));
        ud_stmt.setString(2, rs.getString("id"));

        boolean ok = ud_stmt.executeQuery().first()?true:false;

        pstmt.close();
        right_stmt.close();
        ud_stmt.close();
        return ok;
    }
}
