package launch;

import data.RightManager.Right;
import data.RightManager.Rights;
import data.UserManager.User;
import data.UserManager.Users;
import data.transaction.UpdateActivity;
import database.connect;
import database.credentials;
import externalFunctions.user_rights;

import javax.management.relation.Role;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class launch {
    public static void main(String[] args)
    {
        ArrayList<String> Logins = new ArrayList<>();
        ArrayList<String> droits = new ArrayList<>();

        try {

            Connection db = new connect(
                    credentials.getDbname(),
                    credentials.getDbHost(),
                    credentials.getDbUser(),
                    credentials.getDbpass()
            ).getDb();

            System.out.println("DATABASE: OK");

            try
            {
                Users U = new Users(db);
                for (User i:U.getUtilisateurs())
                {
                    Logins.add(i.getLogin());
                }

                for (User i:U.getUtilisateurs())
                {
                    System.out.println("Uses: "+i.getLogin());
                    System.out.println("Roles: ");
                    for (Right o:i.getRights())
                    {
                        System.out.println("\t"+o.getLibelle());
                    }
                    System.out.println("\n");
                }

                UpdateActivity.VerifActivity(U);
            }
            catch (SQLException e)
            {
                System.out.println("ERROR : COULD NOT CHARGE USER TABLE \n ERROR : "+e);
            }

            try
            {
                Rights R = new Rights(db);
                for (Right i:R.getDroit())
                {
                    droits.add(i.getLibelle());
                }
            }
            catch (SQLException e)
            {
                System.out.println("ERROR : COULD NOT CHARGE RIGHT TABLE \n ERROR_Message : "+e);
            }


            System.out.println("Droits: "+String.join(", ", droits));



            System.out.println("User Right TEST:");

            try
            {
                System.out.println("Should be OK");
                System.out.println(user_rights.isAutorise("vincent","connexion"));

            } catch (SQLException e)
            {
                System.out.println("DAT IS NOT GOOD");
            }

            try
            {
                System.out.println("Should FAIL login");
                System.out.println(user_rights.isAutorise("stephane","connexion"));
            } catch (SQLException e)
            {
                System.out.println("DAT IS GOOD");
            }

            try
            {
                System.out.println("Should FAIL right");
                System.out.println(user_rights.isAutorise("doe","chier"));
            } catch (SQLException e)
            {
                System.out.println("DAT IS GOOD");
            }






            db.close();

        } catch (SQLException e) {
            System.out.println("ERROR : FAIL ON DATABASE \n ERROR : "+e);
        }


    }
}
