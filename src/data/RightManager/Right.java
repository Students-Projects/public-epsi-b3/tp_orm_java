package data.RightManager;

import java.sql.Connection;

public class Right {

    private int id;
    private String libelle;

    public Right(int id, String libelle)
    {
        this.id = id;
        this.libelle = libelle.toLowerCase();
    }

    public Right(String libelle)
    {
        this.libelle = libelle;
    }

    public int getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle.toLowerCase();
    }
}
