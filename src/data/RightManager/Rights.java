package data.RightManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Rights {
    private ArrayList<Right> Droit = new ArrayList<>();
    private Connection db;

    public Rights(Connection db) throws SQLException
    {
        this.db = db;
        String request = "SELECT * FROM Droit";

        PreparedStatement pstmt = db.prepareStatement(request);
        ResultSet rs = pstmt.executeQuery();

        while (rs.next())
        {
            this.Droit.add(new Right(rs.getInt("id"), rs.getString("libelle")));
        }

        pstmt.close();
    }



    public void addUser(Right right) throws SQLException
    {
        String request = "INSERT INTO Droit (libelle) VALUES (?)";

        PreparedStatement pstmt = db.prepareStatement(request);

        pstmt.setString(1, right.getLibelle());

        pstmt.executeUpdate();

        java.sql.ResultSet resultSet = pstmt.getGeneratedKeys();
        if (resultSet.next()) {
            int key = resultSet.getInt(1);
            this.Droit.add(new Right(key, right.getLibelle()));
        }

        pstmt.close();
    }

    public ArrayList<Right> getDroit() {
        return Droit;
    }

    public Connection getDb() {
        return db;
    }
}
