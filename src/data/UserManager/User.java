package data.UserManager;

import data.RightManager.Right;

import java.util.ArrayList;
import java.util.Date;

public class User {

    private int id;
    private String login;
    private Date dateInscription;
    private boolean actif;
    private ArrayList<Right> rights = new ArrayList<>();

    public User(String login, Date dateInscription)
    {
        this(login,dateInscription,true);
    }

    public User(String login, Date dateInscription,Boolean actif) {
        this.login = login.toLowerCase();
        this.dateInscription = dateInscription;
        this.actif = actif;
    }

    public User(int id, String login, Date dateInscription)
    {
        this(id,login,dateInscription,true);
    }

    public User(int id, String login, Date dateInscription, boolean actif) {
        this.id = id;
        this.login = login;
        this.dateInscription = dateInscription;
        this.actif = actif;
    }

    public User(int id, String login, Date dateInscription, ArrayList<Right> R) {
        this(id,login,dateInscription,true, R);
    }

    public User(int id, String login, Date dateInscription, boolean actif, ArrayList<Right> R) {
        this.id = id;
        this.login = login;
        this.dateInscription = dateInscription;
        this.actif = actif;
        this.rights.addAll(R);
    }



    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login.toLowerCase();
    }

    public Date getDateInscription() {
        return dateInscription;
    }

    public long getMillisecondDateInscription()
    {
        return dateInscription.getTime();
    }

    public void setDateInscription(Date dateInscription) {
        this.dateInscription = dateInscription;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Right> getRights() {
        return rights;
    }



    public boolean isAutorise(String right)
    {
        for (Right i:this.getRights())
        {
            if (i.getLibelle().toLowerCase().equals(right.toLowerCase()))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isAutorise(Right right)
    {
        return this.rights.contains(right);
    }
}
