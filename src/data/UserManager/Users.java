package data.UserManager;

import data.RightManager.Right;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class Users {

    private ArrayList<User> utilisateurs = new ArrayList<>();
    private Connection db;

    public Users(Connection db) throws SQLException
    {
        this.db = db;
        String request = "SELECT * FROM Utilisateur";

        PreparedStatement pstmt = db.prepareStatement(request);
        ResultSet rs = pstmt.executeQuery();

        while (rs.next())
        {
            ArrayList<Right> R = new ArrayList<>();

            String RightRequest = "SELECT * FROM Utilisateur_Droit LEFT OUTER JOIN Droit ON id = id_droit WHERE id_utilisateur = ?";
            PreparedStatement rpstmt = db.prepareStatement(RightRequest);

            rpstmt.setInt(1, rs.getInt("id"));
            ResultSet rsR = rpstmt.executeQuery();

            while (rsR.next())
            {
                R.add(new Right(rsR.getInt("id"), rsR.getString("libelle")));
            }

            this.utilisateurs.add(new User(rs.getInt("id"), rs.getString("login"),rs.getDate("dateInscription"), rs.getBoolean("actif"), R));
        }

        pstmt.close();
    }

    public Users(Connection db,User...u) throws SQLException
    {
        for (User i:u)
        {
            this.addUser(i);
        }
        this.db = db;
    }

    public void addUser(User user, String...droit) throws SQLException
    {
        ArrayList<Right> R = new ArrayList<>();

        String right_req = "SELECT id FROM Droit WHERE libelle LIKE ?";

        this.addUser(user, R);
    }

    public void addUser(User user, ArrayList<Right> Droits) throws SQLException
    {
        String request = "INSERT INTO Utilisateur (login, dateInscription, actif) VALUES (?, ?, ?)";

        PreparedStatement pstmt = db.prepareStatement(request);

        pstmt.setString(1, user.getLogin());
        pstmt.setDate(2, new java.sql.Date(user.getMillisecondDateInscription()));
        pstmt.setBoolean(3, user.isActif());

        pstmt.executeUpdate();

        java.sql.ResultSet resultSet = pstmt.getGeneratedKeys();
        if (resultSet.next()) {
            int key = resultSet.getInt(1);
            this.utilisateurs.add(new User(key, user.getLogin(), user.getDateInscription(), user.isActif()));
        }

        pstmt.close();
    }

    public ArrayList<User> getUtilisateurs() {
        return utilisateurs;
    }

    public Connection getDb() {
        return db;
    }
}
