package data.transaction;

import data.UserManager.User;
import data.UserManager.Users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UpdateActivity {
    public static void VerifActivity(Users U) throws SQLException
    {
        Connection db = U.getDb();

        String request = "UPDATE Utilisateur SET actif=false WHERE YEAR(dateInscription)<YEAR(CURRENT_DATE)-10";
        String Srequest = "SELECT * FROM Utilisateur WHERE YEAR(dateInscription)<YEAR(CURRENT_DATE)-10";
        PreparedStatement pstmt = db.prepareStatement(Srequest);
        PreparedStatement Upstmt = db.prepareStatement(request);

        try
        {
            ResultSet rs = pstmt.executeQuery();
            Upstmt.executeUpdate();

            while (rs.next())
            {
                System.out.println(rs.getString("login")+" UPDATED");
                for (User u:U.getUtilisateurs())
                {
                    u.setActif(false);
                }
            }
            db.commit();
        }
        catch (SQLException e)
        {
            db.rollback();
        }
        finally {
            pstmt.close();
            Upstmt.close();
        }
    }


}
